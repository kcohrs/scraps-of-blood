## Shards of Blood

Private Repository for the a prototypical Unity Game with focus on fast-paced Networking. Goal is the creation of a 4 player cooperative twin-stick shooter. Working title is **Shards of Blood** until I can think of something better.

If you find this looking for a reference on implementing Authoriative Controls and Client Prediction, feel free to take a look, but note that the current version is a private WIP with no conformity to any coding standard as testing out things is mostly done via quick botches.
