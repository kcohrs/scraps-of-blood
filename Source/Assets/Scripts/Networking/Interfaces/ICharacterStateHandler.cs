﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NetStack
{
    public interface ICharacterStateHandler
    {
        void OnStateChange(CharacterState newState);
    }

}
