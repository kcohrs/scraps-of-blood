﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NetStack;

public interface ICharacterControl
{
    CharacterInput GetControlAction(GameObject reference);
}


