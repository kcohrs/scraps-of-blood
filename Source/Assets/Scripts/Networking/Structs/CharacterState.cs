﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NetStack
{
    [System.Serializable]
    public struct CharacterState
    {
        public Vector2 position;
        public float angle;

        public Vector2 velocity;
        public int moveNum;
        public int timestamp;

        public static CharacterState Zero
        {
            get
            {
                return new CharacterState
                {
                    position = Vector2.zero,
                    angle = 0f,
                    velocity = Vector2.zero,
                    moveNum = 0,
                    timestamp = 0
                };
            }
        }

        public CharacterState(Vector2 pos, float ang)
        {
            position = pos;
            angle = ang;
            velocity = Vector2.zero;
            moveNum = 0;
            timestamp = 0;
        }

        public static CharacterState Interpolate(CharacterState from, CharacterState to, int clientTick)
        {
         //   Debug.LogWarning("Reimplement CharacterState.Interpolate");
            float t = ((float)(clientTick - from.timestamp)) / (to.timestamp - from.timestamp);
            return new CharacterState
            {
                position = Vector3.Lerp(from.position, to.position, t),
                angle = Mathf.Lerp(from.angle, to.angle, t), 
                moveNum = 0,
                timestamp = 0
            };
        }

        public static CharacterState Extrapolate(CharacterState from, int clientTick)
        {
          //  Debug.LogWarning("Reimplement CharacterState.Extrapolate");
            int t = clientTick - from.timestamp;
            return new CharacterState
            {
                position = from.position + from.velocity * t,
                angle = from.angle + from.angle * t,
                moveNum = from.moveNum,
                timestamp = from.timestamp
            };
        }

        public static CharacterState Move(CharacterController2D controller, CharacterState previous, CharacterInput input, float speed, int timestamp)
        {
            // we need to maybe pass values for collidershape/size? instead of GameObject? advantages?
            var state = new CharacterState
            {
                position = speed * Time.fixedDeltaTime * new Vector2(input.dir.x, input.dir.y) + previous.position,
                angle = input.angle,
                moveNum = previous.moveNum + 1,
                timestamp = timestamp
            };
            // DO SOMEWHAT SOPHISTICATED KINEMATIC COLLISION HERE
            state.position = controller.GetMovePos(state.position);

            Debug.DrawLine(previous.position, state.position, Color.red);
            var timestepInterval = timestamp - previous.timestamp + 1;
            state.velocity = (state.position - previous.position) / timestepInterval;
            return state;
        }
    }
}
