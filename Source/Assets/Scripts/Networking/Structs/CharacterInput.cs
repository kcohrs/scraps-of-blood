﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NetStack
{
    public struct CharacterInput
    {

        public Vector2 dir;
        public float angle;
        public bool jump;
        public int inputNum;

        public bool primaryWeapon;
        public bool secondaryWeapon;

        public static CharacterInput Empty
        {
            get
            {
                return new CharacterInput
                {
                    dir = Vector2.zero,
                    angle = 0f,
                    jump = false,
                    primaryWeapon = false,
                    secondaryWeapon = false,
                    inputNum = -1
                    
                };
            }

        }

        public CharacterInput(Vector2 _dir, float _angle, bool _jump, bool _prim, bool _sec, int _inputNum)
        {
            dir = _dir;
            angle = _angle;
            jump = _jump;
            primaryWeapon = _prim;
            secondaryWeapon = _sec;
            inputNum = _inputNum;
        }

    }
}
