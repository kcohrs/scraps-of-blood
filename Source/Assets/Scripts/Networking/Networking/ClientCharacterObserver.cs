﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

namespace NetStack
{
    public class ClientCharacterObserver : MonoBehaviour, ICharacterStateHandler
    {
        LinkedList<CharacterState> stateBuffer;
        NetworkCharacter character;
        int clientTick = 0;

        // Start is called before the first frame update
        void Start()
        {
            character = GetComponent<NetworkCharacter>();
            stateBuffer = new LinkedList<CharacterState>();
            SetObservedState(character.state);
            AddState(character.state);
        }
        public void ObserverTick()
        {
            clientTick++;
        }

        public void UpdateObserver()
        {
            if (character == null) return; 
            int pastTick = clientTick - character.interpolationDelay;
            var fromNode = stateBuffer.First;
            var toNode = fromNode.Next;
            while (toNode != null && toNode.Value.timestamp <= pastTick)
            {
                fromNode = toNode;
                toNode = fromNode.Next;
                stateBuffer.RemoveFirst();
            }
            SetObservedState(toNode != null ? CharacterState.Interpolate(fromNode.Value, toNode.Value, pastTick) : fromNode.Value);
        }

        public void AddInput(CharacterInput input)
        {
            Debug.LogError("Observer called on a Local Entity: " + gameObject.name);
        }

        public void OnStateChange(CharacterState newState)
        {
            clientTick = newState.timestamp;
            AddState(newState);
        }

        void AddState(CharacterState state)
        {
            if (stateBuffer == null || (stateBuffer.Count > 0 && stateBuffer.Last.Value.timestamp > state.timestamp))
            {
                return;
            }
            stateBuffer.AddLast(state);
        }

        void SetObservedState(CharacterState newState)
        {
            character.SyncState(newState);
        }
    }

}
