﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NetStack;

public class ClientCharacterInput : MonoBehaviour
{
    InputManager manager;
    List<CharacterInput> inputBuffer;
    NetworkCharacter character;

    int currentInput = 0;

    ICharacterControl controller;

    public void Start()
    {
        manager = InputManager.singleton;
        inputBuffer = new List<CharacterInput>();
        character = GetComponent<NetworkCharacter>();
    }

    public void AddController(bool isAi)
    {
        if (!isAi)
        {
            controller = InputManager.singleton;
        } else
        {
            controller = gameObject.AddComponent<NPCActor>();
        }
    }

    public CharacterInput GetControlActions()
    {
        CharacterInput charInput = controller.GetControlAction(gameObject);

        // skip add if nothing changed
        if (inputBuffer.Count != 0 || charInput.dir != Vector2.zero || charInput.angle != character.state.angle)
        {
            charInput.inputNum = currentInput++;
            inputBuffer.Add(charInput);
        }

        if (inputBuffer.Count >= character.inputBufferSize)
        {
            character.CmdMove(inputBuffer.ToArray());
            inputBuffer.Clear();
        }
        return charInput;
    }

    public void UpdateInputs()
    {

        // spool inputs for next fixedupdate
    }
}
