﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;
using Mirror;
using System;

public class SteamConnector : MonoBehaviour
{
    // Start is called before the first frame update
    NetworkManager manager;
    bool init = false;
    Callback<GameRichPresenceJoinRequested_t> one;
    Callback<GameLobbyJoinRequested_t> two;
    Callback<LobbyInvite_t> three;
    Callback<LobbyCreated_t> four;
    Callback<LobbyEnter_t> five;
    ulong lobbyId = 1;

    void Start()
    {
        manager = GetComponent<NetworkManager>();

    }

    private void Update()
    {
        if (!init && SteamManager.Initialized)
        {
            init = true;

            one = Callback<GameRichPresenceJoinRequested_t>.Create(OnInviteRecieve);
            two = Callback<GameLobbyJoinRequested_t>.Create(OnInviteLobbyRecieve);
           // three = Callback<LobbyInvite_t>.Create(OnLobbyInvite);



        }
    }

    /*
    private void OnLobbyInvite(LobbyInvite_t param)
    {
        Debug.Log("Recieved lobby invite");

        manager.networkAddress = param.m_ulSteamIDUser.ToString();
        manager.StartClient();
    }
    */
    private void OnGUI()
    {
        GUI.Box(new Rect(Screen.width - 120, 10, 100, 90), "");
        if (SteamManager.Initialized)
        {
            GUI.Box(new Rect(Screen.width - 120, 10, 100, 90), SteamFriends.GetPersonaName());
            // Make the first button. If it is pressed, Application.Loadlevel (1) will be executed
            if (GUI.Button(new Rect(Screen.width - 110, 40, 80, 20), "Invite Lobby"))
            {
                InviteLobby();
            }
        }
    }

    void InviteLobby()
    {
        if (lobbyId == 1)
        {
            SteamAPICall_t call = SteamMatchmaking.CreateLobby(ELobbyType.k_ELobbyTypeFriendsOnly, 4);
            four = Callback<LobbyCreated_t>.Create(OnLobbyCreated);
        } else
        {
            OpenOverlay();
        }

    }

    private void OnLobbyCreated(LobbyCreated_t param)
    {  
        if (param.m_ulSteamIDLobby != 0)
        {
            lobbyId = param.m_ulSteamIDLobby;
            SteamMatchmaking.SetLobbyData(new CSteamID(param.m_ulSteamIDLobby), "join_id", SteamUser.GetSteamID().m_SteamID.ToString());
            
        }
    }

    private void OpenOverlay()
    {
        SteamFriends.ActivateGameOverlayInviteDialog(new CSteamID(lobbyId));
    }

    private void OnInviteRecieve(GameRichPresenceJoinRequested_t param)
    {
        if (SteamManager.Initialized && param.m_steamIDFriend != null)
        {
            // test this
            manager.networkAddress = param.m_steamIDFriend.m_SteamID.ToString();
            manager.StartClient();
            // Custom stuff
            GameManager.singleton.IsServer = false;
        }
    }

    private void OnInviteLobbyRecieve(GameLobbyJoinRequested_t param)
    {
        SteamMatchmaking.JoinLobby(param.m_steamIDLobby);
        five = Callback<LobbyEnter_t>.Create(OnEnterLobby);
    }

    private void OnEnterLobby(LobbyEnter_t param)
    {
        lobbyId = param.m_ulSteamIDLobby;
        manager.networkAddress = SteamMatchmaking.GetLobbyData(new CSteamID(param.m_ulSteamIDLobby), "join_id");
        manager.StartClient();
       
      //  SteamMatchmaking.LeaveLobby(new CSteamID(param.m_ulSteamIDLobby));
    }
}
