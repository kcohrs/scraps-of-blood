﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;

namespace NetStack
{
    public class NetworkCharacter : NetworkBehaviour
    {
        public float speed = 3f;
        public int inputBufferSize { get; private set; }
        public int interpolationDelay = 12;
        int inputUpdateRate = 60;
        [SyncVar]
        public bool IsAI = true;
        [SyncVar(hook = "OnServerStateChange")]
        public CharacterState state;

        ServerCharacter server;
        ClientCharacterInput input;
        ICharacterStateHandler stateHandler;
        public CharacterValues cvalues;
        public PlayerValues pvalues;
        CharacterController2D body;

        public Transform AimHook;
        public bool canAttack = true;
        public double attackLockTime;

        public GameObject owner = null;

        [SyncVar(hook = "OnNameChange")]
        public string playerName;



        LineRenderer DEBUGline;

        // Start is called before the first frame update
        void Start()
        {
            // the classic isLocalPlayer only applies to the NetworkManager PlayerObject
            // dont do stuff here as it fucks things
            inputBufferSize = (int)(1 / Time.fixedDeltaTime) / inputUpdateRate;
            cvalues = gameObject.GetComponent<CharacterValues>();
            body = gameObject.GetComponent<CharacterController2D>();

            
            if (stateHandler == null)
            {
                stateHandler = gameObject.AddComponent<ClientCharacterObserver>();
            }
        }

        public override void OnStartLocalPlayer()
        {
            base.OnStartLocalPlayer();
        }

        public override void OnStartServer()
        {
            // this is called on Server
            base.OnStartServer();
            server = gameObject.AddComponent<ServerCharacter>();

            if (IsAI)
            {
                input = gameObject.AddComponent<ClientCharacterInput>();
                input.AddController(IsAI);

            }

        }

        public override void OnStartClient()
        {
            base.OnStartClient();
            // this should be only on client
        }

        public override void OnStartAuthority()
        {
            base.OnStartAuthority();
       //     gameObject.GetComponent<SpriteRenderer>().color = Color.cyan;
            
            if (!IsAI)
            {   
                if (stateHandler != null)
                {
                    Destroy((ClientCharacterObserver)stateHandler);
                }
                stateHandler = gameObject.AddComponent<ClientCharacterPredictor>();
                input = gameObject.AddComponent<ClientCharacterInput>();
                input.AddController(IsAI);
            } 
           
            // register for input / or for npc control
        }


        public override bool OnRebuildObservers(HashSet<NetworkConnection> observers, bool initialize)
        {
            return base.OnRebuildObservers(observers, initialize);


        }

        public override void OnNetworkDestroy()
        {
            //Character is getting killed, only called on Clients
            base.OnNetworkDestroy();

            if (hasAuthority)
            {

            }

        }

        private void FixedUpdate()
        {
            if (!canAttack && NetworkTime.time > attackLockTime)
            {
                canAttack = true;
            }

            if (hasAuthority && input != null && stateHandler != null)
            {
                CharacterInput lastInput;
                if (IsAI)
                {
                    lastInput = input.GetControlActions();
                    
                } else
                {
                    lastInput = input.GetControlActions();
                    if (lastInput.inputNum != -1)
                    {
                        // is a valid input
                        ((ClientCharacterPredictor)stateHandler).AddInput(lastInput);
                    }
                } 

                if (lastInput.primaryWeapon && canAttack)
                {
                    // PACK THIS INTO STRUCT AND KEEP UP TO DATE ON NETWORKCHAR
                    CmdShootWeapon(NetworkTime.time, true, state.position, state.angle);
                } else
                {
                    if (lastInput.secondaryWeapon && canAttack)
                    {
                        CmdShootWeapon(NetworkTime.time, false, state.position, state.angle);
                    }
                }

            }
            // apparently, AI spawned via Server.Spawn has NO authority on the server
            if ((!hasAuthority || IsAI ) && stateHandler != null)
            {
                ((ClientCharacterObserver)stateHandler).ObserverTick();
            }

            if (cvalues != null && (playerName != cvalues.PlayerName)) 
            {
                cvalues.UpdateName(playerName);
            }

            if (server != null)
            {
                server.Tick();
            }
        }

        private void Update()
        {
            if (hasAuthority && input != null)
            {
                //gameObject.GetComponent<SpriteRenderer>().color = Color.red;
                input.UpdateInputs();
            } 
            if (!hasAuthority && stateHandler != null)
            {
                ((ClientCharacterObserver)stateHandler).UpdateObserver();
            }
        }

        public void OnNameChange(string newvalue)
        {
            playerName = newvalue;
            if (cvalues != null)
            {
                cvalues.UpdateName(playerName);
            }
        }

        public void SetUpSpawnLocation(Vector3 position, float rotation)
        {
            state = new CharacterState(position, rotation);
            OnServerStateChange(state);
        }

        public void SyncState(CharacterState overrideState)
        {
            Vector2 pos = new Vector2(transform.position.x, transform.position.y);
            body.Move(overrideState.position - pos);

            if (AimHook != null)
            {
                AimHook.eulerAngles = new Vector3(0, 0, overrideState.angle);
            }
        }

        public void OnServerStateChange(CharacterState serverState)
        {
            state = serverState;
            if (stateHandler != null)
            {
                stateHandler.OnStateChange(state);
            }
        }


        public void OnCharacterDeath()
        {
            if (hasAuthority && !IsAI && stateHandler != null)
            {
                if (owner != null)
                {
                    owner.GetComponent<NetworkPlayer>().characterIsDead = true;
                }
                ((ClientCharacterPredictor)stateHandler).PrepareForDeletion();
            }
        }

        #region Commands

        [Command(channel = 0)]
        public void CmdMove(CharacterInput[] inputs)
        {
            server.Move(inputs);
        }

        [Command(channel = 0)]
        public void CmdKillCharacter()
        {
            OnCharacterDeath();
            RpcKillCharacter();
            SnapShotManager.singleton.RemoveObject(gameObject);
            NetworkServer.Destroy(gameObject);
        }

        [Command(channel = 0)]
        public void CmdShootWeapon(double clientTime, bool isPrimary, Vector2 position, float direction)
        {
            if (isPrimary)
            {
                canAttack = false;
                attackLockTime = NetworkTime.time + 0.3 - (NetworkTime.rtt / 2);
                RpcDisplayWeapon(position, direction, NetworkTime.time + 0.3);
                SnapShotManager.singleton.CheckForHitRaycast(clientTime, gameObject, position, direction);
            } else
            {

            }

        }
        #endregion

        #region Rpc
        [ClientRpc]
        public void RpcDisplayWeapon(Vector3 position, float direction, double lockTime)
        {
            canAttack = false;
            attackLockTime = lockTime - NetworkTime.rtt;

            // EDIT THIS
            if (DEBUGline == null)
            {
                DEBUGline = gameObject.AddComponent<LineRenderer>();
            }

            DEBUGline.sortingLayerName = "OnTop";
            DEBUGline.sortingOrder = 5;
            DEBUGline.positionCount = 2;
            DEBUGline.SetPosition(0, position);
            DEBUGline.SetPosition(1, position + (((Quaternion.Euler(0, 0, direction) * Vector3.up) * 1000)));
            DEBUGline.startWidth = 0.05f;
            DEBUGline.endWidth = 0.05f;
            DEBUGline.useWorldSpace = true;
        }

        [ClientRpc]
        public void RpcDamage(int newHealth)
        {
            // this should prolly be renamed "updateValues" or similar
            cvalues.health = newHealth;
        }

        [ClientRpc]
        public void RpcKillCharacter()
        {
            OnCharacterDeath();
        }

        [ClientRpc]
        public void RpcSetOwner(uint netId)
        {
            owner = NetworkIdentity.spawned[netId].gameObject;
            owner.GetComponent<NetworkPlayer>().SetOwnedObject(this);
        }

        #endregion
    }

}
