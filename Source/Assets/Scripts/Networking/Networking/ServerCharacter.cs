﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;

namespace NetStack
{
    public class ServerCharacter : MonoBehaviour
    {
        public int serverTick = 0;

        NetworkCharacter character;
        CharacterController2D controller;
        Queue<CharacterInput> inputBuffer;

        private int movesMade;
        private Vector2 position;

        public void Start()
        {
            inputBuffer = new Queue<CharacterInput>();
            character = GetComponent<NetworkCharacter>();
            controller = GetComponent<CharacterController2D>();

            // no need to set state from here as set by NetworkPlayer/Spawner
        }

        public void Move(CharacterInput[] inputs)
        {
            foreach (CharacterInput input in inputs)
            {
                inputBuffer.Enqueue(input);
            }          
        }

        public void Tick()
        {
            serverTick++;

            if (movesMade > 0)
            {
                movesMade--;
            }  
            if (movesMade == 0)
            {
                CharacterState state = character.state;
                while (movesMade < character.inputBufferSize && inputBuffer.Count > 0)
                {
                    state = CharacterState.Move(controller, state, inputBuffer.Dequeue(), character.speed, serverTick);
                    position = new Vector2(transform.position.x, transform.position.y); 
                    controller.Move(state.position - position);

                    if (GameManager.singleton.IsServer)
                    {
                        SnapShotManager.singleton.AddObjectState(gameObject, gameObject.GetComponent<SpriteAnimator>().UsedHitHurtBoxes);
                    }

                    movesMade++;
                }
                if (movesMade > 0)
                {
                    state.position = transform.position;
                    character.state = state;
                    character.OnServerStateChange(state);
                }
            }
        }
    }

}

