﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace NetStack
{
    public class ClientCharacterPredictor : MonoBehaviour, ICharacterStateHandler
    {

        LinkedList<CharacterInput> pendingInputs;
        NetworkCharacter character;
        CharacterController2D controller;
        public CharacterState predictedState;
        private CharacterState lastServerState = CharacterState.Zero;
        GameObject cam; 

        void Start()
        {
            // we know that this is only called on the local player EVER
            cam = CameraManager.singleton.AddGameplayCamera();
            cam.GetComponent<SmoothCamera2D>().RegisterTarget(gameObject.transform);


            lastServerState.position = transform.position;
            lastServerState.angle = transform.rotation.eulerAngles.z;
            predictedState = lastServerState;
            pendingInputs = new LinkedList<CharacterInput>();
            character = GetComponent<NetworkCharacter>();
            controller = GetComponent<CharacterController2D>();
        }

        public void AddInput(CharacterInput input)
        {
            pendingInputs.AddLast(input);
            ApplyInput(input);
            character.SyncState(predictedState);
        }

        void ApplyInput(CharacterInput input)
        {
            predictedState = CharacterState.Move(controller, predictedState, input, character.speed, 0);
        }

        public void OnStateChange(CharacterState newState)
        {
            if (newState.timestamp > lastServerState.timestamp)
            {
                while (pendingInputs.Count > 0 && pendingInputs.First.Value.inputNum <= newState.moveNum)
                {
                    pendingInputs.RemoveFirst();
                }
                predictedState = newState;
                lastServerState = newState;
                UpdatePredictedState();
            }
        }

        void UpdatePredictedState()
        {
            foreach (CharacterInput input in pendingInputs)
            {
                ApplyInput(input);
            }
            character.SyncState(predictedState);
        }

        public void PrepareForDeletion()
        {

            GameObject newcam = CameraManager.singleton.AddGameplayCamera();
            newcam.transform.position = cam.transform.position;
            newcam.transform.parent = NetworkPlayer.localPlayer.transform;
            cam.SetActive(false);
        }
    }
}