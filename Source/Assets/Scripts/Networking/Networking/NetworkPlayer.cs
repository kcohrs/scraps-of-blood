﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using Steamworks;

namespace NetStack
{
    public class NetworkPlayer : NetworkBehaviour, ICharacterOwner
    {
        public GameObject NetworkCharacterPrefab;

        public static NetworkPlayer localPlayer;

        [SyncVar]
        public string playerName = "";

        public GameObject playerCharacterObject;
        public NetworkCharacter playerCharacter;

        public bool characterIsDead = true;

        // Start is called before the first frame update
        void Start()
        {
            DontDestroyOnLoad(gameObject);
            if (isLocalPlayer)
            {
                CmdSpawnPlayerCharacter();
                localPlayer = this;
            }
        }

        private void Update()
        {
            if (isLocalPlayer && playerName == "" && SteamManager.Initialized)
            {
                CmdSetName(SteamFriends.GetPersonaName());
            }
            // maybe do time based loops here, for example on metadata
        }

        private void OnGUI()
        {
            if (isLocalPlayer)
            {
                if (playerCharacter != null && playerCharacter.cvalues != null)
                {
                    GUI.Box(new Rect(Screen.width - 100, Screen.height - 50, 100, 50), "Health: " + playerCharacter.cvalues.health);
                }
                if (playerCharacter == null)
                {
                    GUI.Box(new Rect(Screen.width - 100, Screen.height - 50, 100, 50), "Youre dead\nR to respawn");
                }

            }
        }

        public override void OnNetworkDestroy()
        {
            GameManager.singleton.isPlaying = false;
        }

        [Command]
        void CmdSetName(string name)
        {
            playerName = name;
            playerCharacter.playerName = name;
            
        }

        [Command]
        public void CmdSpawnPlayerCharacter()
        {
            Vector3 pos = GameManager.singleton.GetSpawnLocation();
            float rot = 0f; // Random.Range(0, 360f);
            if (!ClientScene.prefabs.ContainsValue(NetworkCharacterPrefab))
            {
                ClientScene.RegisterPrefab(NetworkCharacterPrefab);
            }

            playerCharacterObject = Instantiate(NetworkCharacterPrefab, pos, Quaternion.Euler(0, 0, rot), transform);

            playerCharacter = playerCharacterObject.GetComponent<NetworkCharacter>();
            playerCharacter.owner = gameObject;
            playerCharacter.SetUpSpawnLocation(pos, rot);
            playerCharacter.IsAI = false;

            // NetworkServer.AddPlayerForConnection(connectionToClient, playerCharacterObject);
            NetworkServer.SpawnWithClientAuthority(playerCharacterObject, connectionToClient);

            playerCharacter.RpcSetOwner(this.netId);
            // MOVE THIS TO GAMEMANAGER EVENTUALLY?
            GameManager.singleton.isPlaying = true;
            characterIsDead = false;
        }

        public void SetOwnedObject(NetworkCharacter obj)
        {
            playerCharacterObject = obj.gameObject;
            playerCharacter = obj;
        }
    }
}
