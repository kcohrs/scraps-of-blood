﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;
using UnityEngine.UI;
using NetStack;

public class CharacterValues : MonoBehaviour
{
    public int health;
    public string PlayerName;
    public bool displayStats = false;
    NetworkCharacter character;
    public TextMesh nameDisplay;

    // Start is called before the first frame update
    void Start()
    {
        character = GetComponent<NetworkCharacter>();
        PlayerName = "unknown";
    }

    // Update is called once per frame
    void Update()
    {
        // in the future, do this on occaison? listen for most relevant events?

    }

    public void UpdateName(string newname)
    {
        PlayerName = newname;
        if (displayStats)
        {
            nameDisplay.text = newname;
        }
    }

    public void ApplyDamage(int damage)
    {
        health -= damage;
        character.RpcDamage(health);
        if (health <= 0)
        {
            character.CmdKillCharacter();
        }
        
        
        
    }


}
