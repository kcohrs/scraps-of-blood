﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using NetStack;

public class DEBUGdamage : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (NetworkManager.isServer)
        {
            Vector2 center = new Vector2(transform.position.x, transform.position.y);

            var hit = Physics2D.OverlapAreaAll(center - new Vector2(0.5f, 0.5f), center + new Vector2(0.5f, 0.5f));
            foreach (Collider2D cd in hit)
            {
                if (cd.gameObject.GetComponent<NetworkCharacter>() != null)
                {
                    cd.gameObject.GetComponent<NetworkCharacter>().cvalues.ApplyDamage(1);
                }
            }
        }
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
    }
    
}
