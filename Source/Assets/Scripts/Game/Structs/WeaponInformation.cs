﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct WeaponInformation 
{
    // make this enum once planned
    public int type;
    public float damage;
    public float range;
}
