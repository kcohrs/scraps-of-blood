﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/HitscanTest", order = 1)]
public class HitScanWeaponValues : ScriptableObject
{
   
    public struct HitScanWeaponValue
    {
        public int damage;
        public float shottime;

    }


}
