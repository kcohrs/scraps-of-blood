﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager singleton;
    public bool isPlaying = false;

    public GameObject NPCManagerPrefab;
    public GameObject ProjectileManagerPrefab;
    public GameObject CameraManagerPrefab;

    ProjectileManager projectileManager;
    NPCTestSpawner spawner;
    CameraManager cameras;
    SnapShotManager snapshotManager;

    public bool IsServer
    {
        get { return _isServer; }
        set
        {
            _isServer = value;
            if (_isServer)
            {
                SetUpServer();
            } else
            {
                SetUpClient();
            }
            SetUpShared();
        }
    }

    private bool _isServer;


    // Start is called before the first frame update
    void Start()
    {
        if (GameManager.singleton == null)
        {
            singleton = this;
            DontDestroyOnLoad(this);
            SceneManager.sceneLoaded += OnSceneLoaded;
        } else
        {
            Destroy(gameObject);
            return;
        }
       


    }

    private void FixedUpdate()
    {
        if (NetStack.NetworkPlayer.localPlayer != null && NetStack.NetworkPlayer.localPlayer.characterIsDead)
        {
            isPlaying = false;
        }
    }

    private void OnSceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        foreach(HitBox g in transform.GetComponentsInChildren<HitBox>())
        {
            Destroy(g.gameObject);
        }
    }

    internal Vector3 GetSpawnLocation()
    {
        // in the future load last spawn slot, get 1 - 4 permutations around it
        return new Vector3(3,3,0);
    }

    void SetUpServer()
    {
        spawner = Instantiate(NPCManagerPrefab, transform).GetComponent<NPCTestSpawner>();
        snapshotManager = gameObject.AddComponent<SnapShotManager>();
        projectileManager = Instantiate(ProjectileManagerPrefab, transform).GetComponent<ProjectileManager>();
        NetworkServer.Spawn(projectileManager.gameObject);
    }

    void SetUpClient()
    {

    }

    void SetUpShared()
    {
    }


    public void OnHostReady()
    {

    }

    public void ResetManager()
    {
        if (spawner != null)
        {
            Destroy(snapshotManager);
            Destroy(spawner.gameObject);
            Destroy(projectileManager.gameObject);
            CameraManager.singleton.RemoveCurrentCamera();
        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
