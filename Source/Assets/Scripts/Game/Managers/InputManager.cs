﻿using System.Collections;
using System.Collections.Generic;
using NetStack;
using UnityEngine;
using Mirror;


public class InputManager : MonoBehaviour, ICharacterControl
{
    public static InputManager singleton;
    private GameObject lastControlled;

    private CharacterInput lastInput;

    // Start is called before the first frame update
    void Start()
    {
        singleton = this;
        lastInput = CharacterInput.Empty;
    }

    private void Update()
    {
        if (!GameManager.singleton.isPlaying && NetStack.NetworkPlayer.localPlayer != null && NetStack.NetworkPlayer.localPlayer.characterIsDead)
        {
            // these should be managed in one centralized location?
            if (Input.GetKeyDown(KeyCode.R))
            {
                NetStack.NetworkPlayer.localPlayer.CmdSpawnPlayerCharacter();
            }
        }

        if (GameManager.singleton.isPlaying)
        {
            UpdateInput();
        } else
        {
            lastInput = CharacterInput.Empty;
        }
    }

    CharacterInput UpdateInput()
    {
        
        lastInput.dir = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")).normalized;
        if (lastControlled)
        {
            Vector3 dir = Input.mousePosition - Camera.main.WorldToScreenPoint(lastControlled.transform.position);
            float rotangle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            lastInput.angle = Mathf.Round(Quaternion.AngleAxis(rotangle - 90f, Vector3.forward).eulerAngles.z);
        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            lastInput.primaryWeapon = true;
        }
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            lastInput.secondaryWeapon = true;
        }

        // PRIMARY / SECONDARY WEAPONRY HERE
        return lastInput;
    }

    public CharacterInput GetControlAction(GameObject reference)
    {
        lastControlled = reference;
        CharacterInput current = UpdateInput();

        lastInput = CharacterInput.Empty;
        return current;
    }

  
}
