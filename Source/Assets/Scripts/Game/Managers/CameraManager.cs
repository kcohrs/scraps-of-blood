﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraManager : MonoBehaviour
{
    public static CameraManager singleton;

    public int activeCameras = 0;
    // prolly clean this up sometime
    public GameObject StaticCameraPrefab;
    public GameObject IngameCameraPrefab;

    GameObject currentCamera;

    List<GameObject> cameraStack;

    // Start is called before the first frame update
    void Start()
    {
        if (singleton != null) return;
        singleton = this;
        SceneManager.sceneUnloaded += OnSceneUnloaded;
        SceneManager.sceneLoaded += OnSceneLoaded;
        cameraStack = new List<GameObject>();
        // always keep the main camera
        cameraStack.Add(Instantiate(StaticCameraPrefab));
        DontDestroyOnLoad(cameraStack[0]);
        UpdateCameras();
    }

    private void OnSceneUnloaded(Scene scene)
    {
        UpdateCameras();
    }

    public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        UpdateCameras();
    }



    public GameObject AddGameplayCamera()
    {
        cameraStack.Add(Instantiate(IngameCameraPrefab));
        UpdateCameras();
        return currentCamera;
    }

    public void RemoveCurrentCamera()
    {
        Destroy(cameraStack[cameraStack.Count - 1]);
        cameraStack.RemoveAt(cameraStack.Count-1);
        UpdateCameras();
    }

    public void UpdateCameras()
    {
        cameraStack.RemoveAll(item => item == null);
        if (Camera.allCamerasCount > cameraStack.Count)
        {
            foreach(Camera c in Camera.allCameras)
            {
                if (!cameraStack.Contains(c.gameObject)) cameraStack.Add(c.gameObject);
            }
        }
        activeCameras = cameraStack.Count;
        currentCamera = cameraStack[cameraStack.Count - 1];
        currentCamera.SetActive(true);
        for (int i = 0; i < cameraStack.Count-1; i++ )
        {
            cameraStack[i].SetActive(false);
        }
        
    }

}
