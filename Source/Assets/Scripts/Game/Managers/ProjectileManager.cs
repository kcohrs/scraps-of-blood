﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;

public class ProjectileManager : NetworkBehaviour
{
    ProjectileManager singleton;

    // this is LOCAL ids
    public int projectileIndex = 0;
    List<Tuple<int, GameObject>> activeProjectiles;

    // Start is called before the first frame update
    void Start()
    {
        singleton = this;

        activeProjectiles = new List<Tuple<int, GameObject>>();
    }

    public void AddProjectile(GameObject projectile)
    {
        activeProjectiles.Add(new Tuple<int, GameObject>(projectile.GetComponent<Projectile>().projectileId, projectile));
    }

    
    [ClientRpc]
    public void RpcSpawnProjectile(double time, Vector3 pos, float rot, int type)
    {
        // adjust all of these in the future

        
    }

    [ClientRpc]
    public void RpcRemoveProjectile(int id)
    {
        foreach (Tuple<int, GameObject> projectile in activeProjectiles.FindAll(item => item.Item1 == id))
        {
            Destroy(projectile.Item2);
        }
        activeProjectiles.RemoveAll(item => item.Item1 == id);
    }

}
