﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using Mirror;

public class SnapShotManager : MonoBehaviour
{
    public static SnapShotManager singleton;
    public static int MAXSAVEDFRAMES = 60;

    List<TrackedObject> savedObjects;
    List<GameObject> colliderPool;
    List<Tuple<GameObject, GameObject>> colliderUsed;

    Scene snapshotScene;
    PhysicsScene2D physics2D;

    void Start()
    {
        singleton = this;
        savedObjects = new List<TrackedObject>();
        colliderPool = new List<GameObject>();
        colliderUsed = new List<Tuple<GameObject, GameObject>>();
    }

    public void AddObjectState(GameObject reference, List<GameObject> usedHitBoxes)
    {
        SnapShot newSnap = new SnapShot();
        newSnap.timestamp = NetworkTime.time;
        newSnap.colliders = new List<ColliderValues>();
        foreach (GameObject box in usedHitBoxes)
        {
            if (!box.GetComponent<HitBox>().isHurtBox)
            {
                newSnap.colliders.Add(new ColliderValues(box.transform.position, box.transform.rotation.eulerAngles, box.transform.localScale));
            }
        }
        TrackedObject obj = savedObjects.Find(item => item.reference == reference);
        if (obj.timestamps != null)
        {
            if(obj.timestamps.Count >= MAXSAVEDFRAMES)
            {
                obj.timestamps.RemoveAt(0);
            }
            obj.timestamps.Add(newSnap);
        } else
        {
            TrackedObject newObj = new TrackedObject();
            newObj.reference = reference;
            newObj.timestamps = new List<SnapShot>();
            newObj.timestamps.Add(newSnap);
            savedObjects.Add(newObj);
        }
    }

    public void RemoveObject(GameObject reference)
    {
        TrackedObject found = savedObjects.Find(item => item.reference == reference);
        if (found.reference != null)
        {
            savedObjects.Remove(found);
        }
    }

    public void CheckForHitRaycast(double timestamp, GameObject origin, Vector3 position, float direction)
    {

        for (int i = 0; i < savedObjects.Count; i++)
        {
            if (savedObjects[i].reference != origin)
            {
                for (int j = savedObjects[i].timestamps.Count - 1; j >= 0; j--)
                {
                    if (j == 0 || savedObjects[i].timestamps[j].timestamp <= timestamp)
                    {
                        for (int k = 0; k < savedObjects[i].timestamps[j].colliders.Count; k++)
                        {
                            if (colliderPool.Count == 0)
                            {
                                GameObject collider = new GameObject();
                                collider.name = "_snapshot";
                                collider.transform.position = savedObjects[i].timestamps[j].colliders[k].colliderPos;
                                collider.transform.rotation = Quaternion.Euler(savedObjects[i].timestamps[j].colliders[k].colliderRot);
                                collider.transform.localScale = savedObjects[i].timestamps[j].colliders[k].colliderScale;
                                collider.layer = 10;
                                collider.AddComponent<BoxCollider2D>();
                                collider.GetComponent<BoxCollider2D>().size = new Vector2(1f, 1f);
                                collider.transform.parent = transform;
                                colliderUsed.Add(new Tuple<GameObject, GameObject>(savedObjects[i].reference, collider));
                            } else
                            {
                                GameObject collider = colliderPool[0];
                                colliderPool.RemoveAt(0);
                                collider.transform.position = savedObjects[i].timestamps[j].colliders[k].colliderPos;
                                collider.transform.rotation = Quaternion.Euler(savedObjects[i].timestamps[j].colliders[k].colliderRot);
                                collider.transform.localScale = savedObjects[i].timestamps[j].colliders[k].colliderScale;
                                collider.layer = 10;
                                collider.SetActive(true);
                                colliderUsed.Add(new Tuple<GameObject, GameObject>(savedObjects[i].reference, collider));
                            }
                        }
                        break;
                    }
                }
            }
        }
       
        LayerMask mask = LayerMask.GetMask("HitCheckLayer");
        // this will need range passed soon
        RaycastHit2D hit = physics2D.Raycast(position, Quaternion.Euler(0, 0, direction) * Vector3.up, 1000, mask);
        if (hit.collider)
        {
            // this does for now but prolly rather return hit gameobject and clean up
            Tuple<GameObject, GameObject> hitTuple = colliderUsed.Find(item => item.Item2 == hit.collider.gameObject);
            if (hitTuple != null && hitTuple.Item1.GetComponent<CharacterValues>() != null)
            {
                // SAME FOR DAMAGE
                Debug.Log("DAMAGE to " + hitTuple.Item1.name + " at " + NetworkTime.time);
                hitTuple.Item1.GetComponent<CharacterValues>().ApplyDamage(5);
            }
        }
        for (int i = 0; i < colliderUsed.Count; i++)
        {
            colliderUsed[i].Item2.SetActive(false);
            
            colliderPool.Add(colliderUsed[i].Item2);
        }
        colliderUsed.Clear();
    }

    private struct TrackedObject
    {
        public GameObject reference;
        public List<SnapShot> timestamps;
    }

    private struct SnapShot
    {
        // if need be add additional features such as collider bounds
        public double timestamp;

        public List<ColliderValues> colliders;
    }

    private struct ColliderValues
    {
        public Vector3 colliderPos;
        public Vector3 colliderRot;
        public Vector3 colliderScale;

        public ColliderValues(Vector3 pos, Vector3 rot, Vector3 scal)
        {
            colliderPos = pos;
            colliderRot = rot;
            colliderScale = scal;
        }
    }
}
