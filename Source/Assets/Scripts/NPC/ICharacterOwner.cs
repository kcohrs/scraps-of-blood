﻿using UnityEngine;
using NetStack;
using Mirror;

public interface ICharacterOwner 
{
    void SetOwnedObject(NetworkCharacter obj);
    // might have some uses in the future
}