﻿using System.Collections;
using System.Collections.Generic;
using NetStack;
using UnityEngine;

public class NPCActor : MonoBehaviour, ICharacterControl
{

    Vector3 origin;
    public bool mode = false;

    public CharacterInput GetControlAction(GameObject reference)
    {
        if (transform.position.x > origin.x + 3)
        {
            mode = false;
        }
        if (transform.position.x < origin.x - 3)
        {
            mode = true;
        }
        Vector2 move = Vector2.zero;
        if (mode)
        {
            move = (Vector2.right * 0.3f);
        }
        else
        {
            move = (Vector2.left * 0.3f);
        }
        return new CharacterInput(move, 0, false, false, false, -1);
    }

    // Start is called before the first frame update
    void Start()
    {
        origin = transform.position;   
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
