﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using NetStack;

public class NPCTestSpawner : MonoBehaviour, ICharacterOwner
{
    public GameObject NPCPrefab;
    GameObject spawned;
    public static NPCTestSpawner singleton;

    // Start is called before the first frame update
    void Start()
    {
        singleton = this;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (spawned == null && NetworkClient.connection.isReady)
        {
            spawned = Instantiate(NPCPrefab, Vector3.zero, Quaternion.identity, transform);
            spawned.GetComponent<NetworkCharacter>().SetUpSpawnLocation(Vector3.zero, 0f);
            spawned.GetComponent<NetworkCharacter>().IsAI = true;

            NetworkServer.Spawn(spawned);
        }
           
          
    }

    public void SetOwnedObject(NetworkCharacter obj)
    {
        spawned = obj.gameObject;
    }
}
