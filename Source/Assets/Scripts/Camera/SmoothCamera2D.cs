﻿using UnityEngine;
using System.Collections;
using System;

public class SmoothCamera2D : MonoBehaviour
{

	public float dampTime = 0.15f;
	public float maxDistance = 2f;
	public int distanceDivisor = 4;
	private Vector3 velocity = Vector3.zero;
	public Transform target;


    // Update is called once per frame
    void Update()
	{
		if (target != null)
		{

          //  Debug.LogWarning("SMOOTHCAMERA needs to be reimplemented!");

			Vector3 mousePosition = GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition);

			float dist = Mathf.Clamp(Vector3.Distance(target.position, new Vector3(mousePosition.x, mousePosition.y, target.position.z)) / distanceDivisor, 0f, maxDistance);
			Vector3 targetPos = Vector3.MoveTowards(target.position, new Vector3(mousePosition.x, mousePosition.y, target.position.z), dist);

			Vector3 point = GetComponent<Camera>().WorldToViewportPoint(targetPos);
			Vector3 delta = targetPos - GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
			Vector3 destination = transform.position + delta ;
			transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);

		}
	}

    internal void RegisterTarget(Transform trans)
    {
        target = trans;
        transform.parent = trans;
    }
}