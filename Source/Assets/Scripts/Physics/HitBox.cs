﻿/// <summary>
/// This is your hurt/hit box. It goes on the box prefab.
/// You shouldn't need to mess with this too much.
/// 
/// Place this on the hit/hurt box prefab.
/// </summary>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Collider2D))]
public class HitBox : MonoBehaviour
{

    public bool isHurtBox = false;
    public GameObject OwnerGameObjectReference; //What game object owns this hit/hurt box?
    public bool isInUse = false;

    public bool isDebug = true;

    public void Make()
    {
        if (isDebug)
        {
            if (isHurtBox)
            {
                this.GetComponent<SpriteRenderer>().color = new Color(1, 0, 0, 0.5f);
            }
            else
            {
                this.GetComponent<SpriteRenderer>().color = new Color(0, 1, 0, 0.5f);
            }
        }
        else
        {
            this.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<HitBox>() != null)
        {
            // we green, they red
            if (!isHurtBox && collision.gameObject.GetComponent<HitBox>().isHurtBox)
            {
                Debug.Log("HIT");
                
            }
        }
    }



}
