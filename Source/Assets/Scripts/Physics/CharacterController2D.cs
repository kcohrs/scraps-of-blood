﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections.Generic;
using UnityEditor;

public class CharacterController2D : MonoBehaviour
{
    float radius = 0.5f;

    public void Start()
    {
        radius = GetComponent<CircleCollider2D>().radius;
    }

    public Vector2 GetMovePos(Vector2 newpos)
    {
      //  Debug.LogWarning("CHARACTERCONTROLLER2D NEEDS TO BE REIMPLEMENTED");
        Collider2D[] results = new Collider2D[20];

        Vector2 finalpos = newpos;
        bool contact = true;
        int iterations = 0;
        while (iterations < 2 && contact)
        {
            // do pushback, recursive?
            Array.Clear(results, 0, 20);

            LayerMask mask = LayerMask.GetMask("World", "Characters");
            Physics2D.OverlapCircleNonAlloc(finalpos, radius, results, mask);

            iterations++;
            foreach (Collider2D col in results)
            {
                // filter redundant ghosts
                if (col == null) continue;

                if (col.transform.root == transform.root)
                {
                    continue;
                }

                Vector2 position = finalpos;
                Vector2 contactPoint =col.ClosestPoint(position);
                bool contactPointSuccess = false;
                if (contactPoint != position|| col.bounds.Contains(position))
                {
                    contactPointSuccess = true;
                }


                //Colliders.ClosestPointOnSurface(col, position, radius, out contactPoint);
                //SuperCollider.

                if (!contactPointSuccess)
                {
                    continue;
                }

                //DebugDraw.DrawMarker(contactPoint, 2.0f, Color.cyan, 0.0f, false);

                Vector2 v = contactPoint - position;
                if (v != Vector2.zero)
                {
                    // Cache the collider's layer so that we can cast against it
                    int layer = col.gameObject.layer;

                    int templayerID = SortingLayer.NameToID("TempCollision");
                    int templayer = LayerMask.GetMask("TempCollision");
                    col.gameObject.layer = templayerID;

                    // Check which side of the normal we are on
                    bool facingNormal = Physics.SphereCast(new Ray(position, v.normalized), 0.01f, v.magnitude + 0.01f, templayer);

                    col.gameObject.layer = layer;

                    // Orient and scale our vector based on which side of the normal we are situated
                    if (!facingNormal)
                    {
                        if (Vector2.Distance(position, contactPoint) < radius)
                        {
                            v = v.normalized * (radius - v.magnitude) * -1;
                        }
                        else
                        {
                            // A previously resolved collision has had a side effect that moved us outside this collider
                            continue;
                        }
                    }
                    else
                    {
                        v = v.normalized * (radius + v.magnitude);
                    }
                    // Debug.Log("Pushing due to " + col.gameObject.name + " : " + v + " (dist: " + (contactPoint - position) + ")");
                    //Debug.Break();
                    contact = true;
                    finalpos += v;

                }
            }
        }
        if (Vector2.Distance(newpos, finalpos) > 0)
        {
            //Debug.Log("Moving From " + newpos + " to " + finalpos);
        }

        return finalpos;
    }

    // do similar to charcont move()
    public void Move(Vector3 delta)
    {
        Vector2 pos = transform.position + delta;

        transform.position = pos;
        //SnapShotManager.manager.AddObjectState(gameObject, gameObject.GetComponent<FF_Animator>().UsedHitHurtBoxes);
    }
}